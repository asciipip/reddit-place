#!/usr/bin/env python3

import collections
import csv
import datetime
import enum
import gzip
import io
import lzma
import math
import mmap
import pathlib
import pickle
import struct
import time

import dask.dataframe as dd
import dateutil.parser
import numpy as np
import pandas as pd
import sqlalchemy as db
import tqdm
import ujson

import place_pb2


DEFAULT_TILE_DATA = 'data/sorted_tiles'
DEFAULT_TILE_JSON = 'data/tiles.json'

PROTOBUF_SIZE_FORMAT = '<I'

# The official start time for /r/place.  Some data was placed before that
# (in testing, I assume); that data just becomes part of the initial
# canvas.
PLACE_START = {
    2017: {
        0: datetime.datetime(2017, 3, 31, 17, 0, 0, tzinfo=datetime.timezone.utc),
    },
    2022: {
        0: datetime.datetime(2022, 4, 1, 12, 44,  0, tzinfo=datetime.timezone.utc),
        1: datetime.datetime(2022, 4, 2, 16, 24,  0, tzinfo=datetime.timezone.utc),
        2: datetime.datetime(2022, 4, 3, 19,  0,  0, tzinfo=datetime.timezone.utc),
        3: datetime.datetime(2022, 4, 3, 19,  0,  0, tzinfo=datetime.timezone.utc),
    },
}
PLACE_END = {
    2017: datetime.datetime(2017, 4, 3, 17,  0, 0, tzinfo=datetime.timezone.utc),
    2022: datetime.datetime(2022, 4, 5,  0, 15, 0, tzinfo=datetime.timezone.utc),
}
# 2022-04-01 12:44:10  First recorded tile
# 2022-04-01 13:14:15  "Welcome to r/place"
# 2022-04-02 16:24:56  First Q1 tile
# 2022-04-02 16:30:32  "Welcome to Day 2 of Place" (first expansion)
# 2022-04-03 19:03:24  "Day 3: This is the final expansion"
# 2022-04-03 19:03:53  First Q3/Q4 tile
# 2022-04-04 17:34:54  "Day 4: Place will end today"
# 2022-04-04 22:47:40  Last non-white tile
# 2022-04-05 00:06:02  "Place has ended"
# 2022-04-05 00:14:00  Last tile placed (note: when was last non-white changed to white?)

CANVAS_SIZE = {
    2017: (1000, 1000),
    2022: (2000, 2000),
}

COLORS = {
    2017: np.array([
        (255, 255, 255, 255),  # 0: white
        (228, 228, 228, 255),  # 1: light gray
        (136, 136, 136, 255),  # 2: gray
        ( 34,  34,  34, 255),  # 3: black
        (255, 167, 209, 255),  # 4: light pink
        (229,   0,   0, 255),  # 5: red
        (229, 149,   0, 255),  # 6: orange
        (160, 106,  66, 255),  # 7: brown
        (229, 217,   0, 255),  # 8: yellow
        (148, 224,  68, 255),  # 9: light green
        (  2, 190,   1, 255),  # 10: dark green
        (  0, 211, 221, 255),  # 11: light blue
        (  0, 131, 199, 255),  # 12: dark blue
        (  0,   0, 234, 255),  # 13: blue
        (207, 110, 228, 255),  # 14: purple
        (130,   0, 128, 255),  # 15: dark purple
        (  0,   0,   0,   0),  # unset
    ], np.uint8),
    2022: np.array([
        (109,   0,  26, 255),  # burgundy
        (190,   0,  57, 255),  # dark red
        (255,  69,   0, 255),  # red
        (255, 168,   0, 255),  # orange
        (255, 214,  53, 255),  # yellow
        (255, 248, 184, 255),  # pale yellow
        (  0, 163, 104, 255),  # dark green
        (  0, 204, 120, 255),  # green
        (126, 237,  86, 255),  # light green
        (  0, 117, 111, 255),  # dark teal
        (  0, 158, 170, 255),  # teal
        (  0, 204, 192, 255),  # light teal
        ( 36,  80, 164, 255),  # dark blue
        ( 54, 144, 234, 255),  # blue
        ( 81, 233, 244, 255),  # light blue
        ( 73,  58, 193, 255),  # indigo
        (106,  92, 255, 255),  # periwinkle
        (148, 179, 255, 255),  # lavender
        (129,  30, 159, 255),  # dark purple
        (180,  74, 192, 255),  # purple
        (228, 171, 255, 255),  # pale purple
        (222,  16, 127, 255),  # magenta
        (255,  56, 129, 255),  # pink
        (255, 153, 170, 255),  # light pink
        (109,  72,  47, 255),  # dark brown
        (156, 105,  38, 255),  # brown
        (255, 180, 112, 255),  # beige
        (  0,   0,   0, 255),  # black
        ( 81,  82,  82, 255),  # dark gray
        (137, 141, 144, 255),  # gray
        (212, 215, 217, 255),  # light gray
        (255, 255, 255, 255),  # white
        (  0,   0,   0,   0),  # unset
    ], np.uint8),
}
COLOR_WHITE_INDEX = {
    2017: 0,
    2022: 31,
}
COLOR_MISSING_INDEX = {
    2017: 16,
    2022: 32,
}
COLORS_LOOKUP = {}
for year, colors in COLORS.items():
    COLORS_LOOKUP[year] = {}
    for i, color in enumerate(colors):
        COLORS_LOOKUP[year][tuple(color)] = i

class FileFormat(enum.Enum):
    HDF5 = '.h5'
    JSON = '.json'
    SQL = '.db'
    CSV = '.csv'
    PICKLE = '.pickle'
    TS_PBF = '.ts.pbf'
    PX_PBF = '.px.pbf'

Placement = collections.namedtuple(
    'Placement', ['ts', 'x', 'y', 'color', 'user_hash', 'mod_action'],
    defaults=('', 0))


###############
### TimedAction


class TimedAction:
    """Just a simple context manager to wrap an action with a progress message.

    Use it like this:

        with TimedAction('message'):
            # do things here

    The above will initially print:

        message...

    When the actions are done, it will update the line to have:

        message... done (aa:bb.cc)

    "aa:bb.cc" is the time elapsed since starting the actions.

    You can pass a "quiet" parameter to suppress the message.
    """
    def __init__(self, message, quiet=False):
        self.message = message
        self.quiet = quiet

    def __enter__(self):
        if not self.quiet:
            self.start_time = time.time()
            print(self.message, '...', sep='', end=' ', flush=True)

    def __exit__(self, type, value, traceback):
        if not self.quiet:
            print('done ({})'.format(format_duration(time.time() - self.start_time)))


def format_duration(seconds):
    remnant = seconds
    result = '{:.2f}'.format(remnant % 60)
    remnant = math.floor(remnant // 60)
    if remnant == 0:
        return result
    result = str(remnant % 60) + ':' + result
    remnant = remnant // 60
    if remnant == 0:
        return result
    result = str(remnant) + ':' + result
    return result


##################################
### datetime/timestamp conversions


def dt_to_ts(dt):
    """Converts a datetime into epoch milliseconds."""
    # Assume UTC
    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=datetime.timezone.utc)
    return int(dt.astimezone(datetime.timezone.utc).timestamp() * 1000)


def ts_to_dt(ts):
    """Converts epoch milliseconds to a datetime."""
    return datetime.datetime.utcfromtimestamp(ts / 1000)\
                            .replace(tzinfo=datetime.timezone.utc)


############################
### File type identification


def get_file_format(path):
    if '://' in str(path):
        return FileFormat.SQL
    path = pathlib.Path(path)
    if '.pbf' in path.suffixes:
        if '.ts' in path.suffixes:
            return FileFormat.TS_PBF
        elif '.px' in path.suffixes:
            return FileFormat.PX_PBF
        else:
            assert False, 'Unknown protobuf format: ' + str(path)
    for fmt in FileFormat:
        if fmt.value in path.suffixes:
            return fmt
    assert False, 'Unknown file format: ' + str(path)


def is_file_h5(path):
    return get_file_format(path) == FileFormat.HDF5


def is_file_json(path):
    return get_file_format(path) == FileFormat.JSON


def is_file_csv(path):
    return get_file_format(path) == FileFormat.CSV


def is_file_pickle(path):
    return get_file_format(path) == FileFormat.PICKLE


def is_file_protobuf_ts(path):
    return get_file_format(path) == FileFormat.TS_PBF


#############################################
### Abstraction over opening compressed files


def file_opener(path):
    """Returns a function to open the file, depending on the file's compression scheme."""
    if path.suffix == '.xz':
        return lzma.open
    elif path.suffix == '.gz':
        return gzip.open
    else:
        return open


#################
### get_data_year


def get_data_year(data):
    if isinstance(data, pd.DataFrame):
        return pd.to_datetime(data.index[0], unit='ms').year
    elif isinstance(data, pathlib.Path):
        return get_data_year_from_file(data)
    elif isinstance(data, str):
        return get_data_year_from_file(pathlib.Path(data))
    else:
        assert False, 'Unknown data type for geting year: ' + str(data)


def get_data_year_from_file(data_file):
    if is_file_h5(data_file):
        df = pd.read_hdf(data_file, 'tiles', columns=['ts'], stop=1)
        return get_data_year(df)
    elif is_file_json(data_file):
        with file_opener(data_file)(data_file, 'r') as f:
            row = ujson.loads(f.readline())
            ts = datetime.datetime.utcfromtimestamp(row['ts'] / 1000)
            return ts.year
    elif is_file_pickle(data_file):
        with file_opener(data_file)(data_file, 'rb') as f:
            row = pickle.load(f)
            ts = datetime.datetime.utcfromtimestamp(row.ts / 1000)
            return ts.year
    elif is_file_protobuf_ts(data_file):
        with file_opener(data_file)(data_file, 'rb') as f:
            header = pb_read_delimited(f, place_pb2.Header)
            return header.year
    elif is_file_csv(data_file):
        with file_opener(data_file)(data_file, 'rt') as f:
            reader = csv.reader(f)
            # First row might be header; skip it
            row = next(reader)
            row = next(reader)
            try:
                ts = datetime.datetime.utcfromtimestamp(int(row[0]) / 1000)
            except ValueError:
                # int conversion failed; assume it's a formatted string
                ts = dateutil.parser.parse(row[0])
            return ts.year
    else:
        raise Exception('Unknown file type: {}'.format(data_file))


################
### get_quadrant


def get_quadrant(x, y, year):
    if year == 2017:
        return 0
    elif year == 2022:
        return x // 1000 + 2 * (y // 1000)
    else:
        assert False, 'Don\'t know quadrants for year {}'.format(year)


#############
### linecount


def linecount(filename, progress=True):
    """Returns the number of lines in the file.

    If progress is a number, use that for the tqdm progress bar level.
    Otherwise, if progress evaluates to True, show a prograss bar.
    Otherwise, be quiet.

    """
    if filename.suffix == '.xz':
        opener = lzma.open
    elif filename.suffix == '.gz':
        opener = gzip.open
    else:
        opener = open
    iterator = opener(filename, 'r')
    if isinstance(progress, bool):
        if progress:
            iterator = tqdm.tqdm(opener(filename, 'r'), desc='counting placements')
    elif isinstance(progress, int):
        iterator = tqdm.tqdm(opener(filename, 'r'), desc='counting placements',
                             position=progress, leave=None)
    return sum(1 for line in iterator)


###########
### read_df

def read_df(data_path, table=None, dask=False, columns=None, where=None):
    file_format = get_file_format(data_path)
    if file_format == FileFormat.HDF5:
        if dask:
            assert where is None
            return dd.read_hdf(data_path, table, columns=columns, sorted_index=True)
        else:
            return pd.read_hdf(data_path, table, columns=columns, where=where)
    elif file_format == FileFormat.SQL:
        return read_df_sql(data_path, table, dask, columns)
    else:
        assert False, 'Don\'t know how to read DataFrame for file format ' + str(file_format)
    #JSON = '.json'
    #CSV = '.csv'
    #PICKLE = '.pickle'
    #TS_PBF = '.ts.pbf'
    #PX_PBF = '.px.pbf'


def read_df_sql(connection_string, table, dask, columns):
    if '://' not in str(connection_string):
        connection_string = 'sqlite://'.format(connection_string)
    engine = db.create_engine(connection_string)
    conn = engine.connect()
    user_hash_query = db.text("""
      SELECT user_hash, count(user_hash)
        FROM "{}"
        GROUP BY user_hash
        ORDER BY count DESC, user_hash""".format(table))
    user_hash_result = conn.execute(user_hash_query)
    user_hash_dtype = pd.CategoricalDtype([r[0] for r in user_hash_result])
    if dask:
        df = dd.read_sql_table(table, engine, index_col='ts', coerce_float=False, columns=columns)
    else:
        if columns is None:
            columns = ['*']
        dtypes = {
            'x': np.uint16,
            'y': np.uint16,
            'color': np.uint8,
            'user_hash': user_hash_dtype,
        }
        df = pd.read_sql_query(
            'SELECT {} FROM "{}"'.format(', '.join(columns), table),
            engine, index_col='ts', coerce_float=False, dtype=dtypes)
        #df = pd.read_sql_table(table, engine, index_col='ts', coerce_float=False, columns=columns)
        #for column, dtype in dtypes.items():
        #    if column in df.columns:
        #        df[column] = df[column].astype(dtype)
    return df


######################
### iterate_placements


def iterate_placements(placement_file, progress=False):
    if is_file_json(placement_file):
        iterator = iterate_placements_json
    elif is_file_pickle(placement_file):
        iterator = iterate_placements_pickle
    elif is_file_protobuf_ts(placement_file):
        iterator = iterate_placements_protobuf
    else:
        raise Exception('Don\'t know how to handle file type: {}'.format(placement_file))
    for p in iterator(placement_file, progress):
        yield p


def iterate_placements_json(placement_file, progress=False):
    with open(placement_file, 'r') as tile_data:
        if progress:
            reader = tqdm.tqdm(tile_data, total=linecount(placement_file), desc='reading placements')
        else:
            reader = tile_data
        try:
            for line in reader:
                yield Placement(**ujson.loads(line))
        finally:
            if progress:
                reader.close()


def iterate_placements_pickle(placement_file, progress=False):
    if progress:
        pb = tqdm.tqdm(desc='reading placements', total=placement_file.stat().st_size)
    with open(placement_file, 'rb') as tile_data:
        try:
            while True:
                yield pickle.load(tile_data)
                if progress:
                    pb.update(tile_data.tell() - pb.n)
        except EOFError:
            pass  # Exit loop
        if progress:
            pb.close()


def iterate_placements_protobuf(placement_file, progress=False):
    if progress:
        pb = tqdm.tqdm(desc='reading placements', total=placement_file.stat().st_size)
    with open(placement_file, 'rb') as tile_data:
        header = pb_read_delimited(tile_data, place_pb2.Header)
        assert header.format == place_pb2.FileFormat.CHRONOLOGICAL
        placement_block = pb_read_delimited(tile_data, place_pb2.PlacementBlock)
        while placement_block is not None:
            last_placement = None
            for placement in placement_block.placements:
                if last_placement is not None:
                    placement.ts += last_placement.ts
                yield placement
                last_placement = placement
            if progress:
                pb.update(tile_data.tell() - pb.n)
            placement_block = pb_read_delimited(tile_data, place_pb2.PlacementBlock)
        if progress:
            pb.close()


#####################################
### Length-delimited protobuf objects


def pb_write_delimited(stream, message):
    serialized = message.SerializeToString()
    length_bytes = struct.pack(PROTOBUF_SIZE_FORMAT, len(serialized))
    stream.write(length_bytes)
    stream.write(serialized)


def pb_read_delimited(stream, cls):
    """Returns a new protobuf object of the given class containing the data
    from the given stream.

    Assumes that the message class is the correct type for the current
    position in the stream.  No verification is performed.

    """
    length_raw = stream.read(struct.calcsize(PROTOBUF_SIZE_FORMAT))
    if len(length_raw) != struct.calcsize(PROTOBUF_SIZE_FORMAT):
        # Probably EOF.
        return None
    message_length = struct.unpack(PROTOBUF_SIZE_FORMAT, length_raw)[0]
    message_raw = stream.read(message_length)
    if len(message_raw) != message_length:
        raise Exception('Incomplete file read')
    message = cls()
    message.ParseFromString(message_raw)
    return message


# Taken from the Pandas documentation
def psql_insert_copy(table, conn, keys, data_iter):
    """
    Execute SQL statement inserting data

    Parameters
    ----------
    table : pandas.io.sql.SQLTable
    conn : sqlalchemy.engine.Engine or sqlalchemy.engine.Connection
    keys : list of str
        Column names
    data_iter : Iterable that iterates the values to be inserted
    """
    # gets a DBAPI connection that can provide a cursor
    dbapi_conn = conn.connection
    with dbapi_conn.cursor() as cur:
        s_buf = io.StringIO()
        writer = csv.writer(s_buf)
        writer.writerows(data_iter)
        s_buf.seek(0)

        columns = ', '.join(['"{}"'.format(k) for k in keys])
        if table.schema:
            table_name = '{}.{}'.format(table.schema, table.name)
        else:
            table_name = table.name

        sql = 'COPY {} ({}) FROM STDIN WITH CSV'.format(table_name, columns)
        cur.copy_expert(sql=sql, file=s_buf)
