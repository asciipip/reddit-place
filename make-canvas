#!/usr/bin/env python3

import argparse
import os
import pathlib
import sys

import dateutil.parser
import pandas as pd
import png
import numpy as np

from common import PLACE_START, COLORS, \
    COLOR_WHITE_INDEX, COLOR_MISSING_INDEX, \
    CANVAS_SIZE, TimedAction, get_data_year


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--tile-data', default=pathlib.Path('tiles.h5'),
                        type=pathlib.Path,
                        help='File containing the tile placement data.  Must be a HDF5 file created by the bundled `import` program.  Default: {}'.format('tiles.h5'))
    parser.add_argument('--scale', '-s', type=int, default=1)
    parser.add_argument('-e', '--end', type=dateutil.parser.parse,
                        help='Date/time (UTC) at which to stop placing pixels.')
    parser.add_argument('output', help='Output filename.')
    args = parser.parse_args()

    if not args.tile_data.is_file():
        print('{} does not exist'.format(args.tile_data), file=sys.stderr)
        exit(1)

    if args.scale < 1:
        print('Scale must be greater than zero.', file=sys.stderr)
        exit(1)

    return args


if __name__ == '__main__':
    args = parse_args()
    year = get_data_year(args.tile_data)

    if args.end is not None:
        args.end = dt_to_ts(args.end)
        query = 'index <= {}'.format(args.end)
    else:
        query = None

    with TimedAction('reading data'):
        df = pd.read_hdf(args.tile_data, 'tiles', where=query,
                         columns=['x', 'y', 'color'])

    canvas = np.full(list(reversed(CANVAS_SIZE[year])),
                     COLOR_WHITE_INDEX[year],
                     np.uint8)

    canvas[df.y, df.x] = df.color

    if args.end is not None:
        for q in PLACE_START[year].keys():
            if args.end < PLACE_START[year][q]:
                for i in range(0, 1000):
                    for j in range(0, 1000):
                        canvas[i + q // 2 * 1000, j + q % 2 * 1000] = \
                            COLOR_MISSING_INDEX[year]

    writer = png.Writer(CANVAS_SIZE[year][0] * args.scale,
                        CANVAS_SIZE[year][1] * args.scale,
                        bitdepth=8,
                        palette=COLORS[year])
    with open(args.output, 'wb') as output:
        writer.write(output, np.kron(canvas, np.ones((args.scale, args.scale),
                                                     np.uint8)))
