#!/usr/bin/env python3

import click
import sqlalchemy as db

from common import TimedAction


#CREATE INDEX ix_opl_details_timestamp_gist on opl_details USING gist("timestamp")
#CREATE INDEX ix_tiles_2022_ts_gist on tiles_2022 USING gist(ts)

@click.command()
@click.help_option('-h', '--help')
@click.option('-d', '--database', required=True,
              help='SQLAlchemy database connection string')
@click.option('--tile-table', default='tiles')
@click.option('--detail-table', default='opl_details')
@click.option('--map-table', default='user_map')
def main(database, tile_table, detail_table, map_table):
    """Uses a PostgreSQL database to correlate tile placements between opl_'s
    usernames and Reddit's user hashes.

    """
    metadata_obj = db.MetaData()
    engine = db.create_engine(database, echo=True)
    tiles = db.Table(tile_table, metadata_obj, autoload_with=engine)
    tile_gist_index_name = 'ix_{}_ts_gist'.format(tiles.name)
    details = db.Table(detail_table, metadata_obj, autoload_with=engine)
    detail_gist_index_name = 'ix_{}_timestamp_gist'.format(details.name)
    if not table_has_index(tiles, tile_gist_index_name):
        index = db.index(tile_gist_index_name, db.func.gist(tiles.c.ts))
        with TimedAction('Creating tile GiST index'):
            index.create(engine)
    if not table_has_index(details, detail_gist_index_name):
        index = db.index(detail_gist_index_name, db.func.gist(details.c.timestamp))
        with TimedAction('Creating detail GiST index'):
            index.create(engine)
    with engine.connect() as conn:
        with TimedAction('Correlating'):
            conn.execute('DROP TABLE IF EXISTS "{}"'.format(map_table))
            stmt = db.text("""
                SELECT DISTINCT ON (user_hash) user_hash, username, count(*)
                  INTO "{}"
                  FROM "{}" t
                    JOIN "{}" o
                      ON int8range(t.ts - 500, t.ts + 500) @> o.timestamp
                         AND t.x % 1000 = o.x
                         AND t.y % 1000 = o.y
                  GROUP BY user_hash, username
                  ORDER BY user_hash, count DESC""".format(
                      map_table, tiles.name, details.name))
            conn.execute(stmt)
    with TimedAction('Creating map indexes'):
        user_map = db.Table(map_table, metadata_obj, autoload_with=engine)
        db.index('ix_{}_username'.format(map_table), user_map.c.username) \
          .create(engine)
        db.index('ix_{}_user_hash'.format(map_table), user_map.c.user_hash) \
          .create(engine)


def table_has_index(table, index_name):
    for index in table.indexes:
        if index.name == index_name:
            return True
    return False


if __name__ == '__main__':
    main()
