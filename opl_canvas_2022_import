#!/usr/bin/env python3

import base64
import collections
import csv
import datetime
import lzma
import pathlib
import tarfile

import click
import numpy as np
import pandas as pd
import png
from tqdm import tqdm

from common import COLORS, CANVAS_SIZE, COLOR_WHITE_INDEX, COLOR_MISSING_INDEX,\
    TimedAction, linecount


PNG_SIGNATURE = b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a'
TAR_SIGNATURE = b'\x75\x73\x74\x61\x72'
TAR_SIGNATURE_OFFSET = 257

Snapshot = collections.namedtuple('Snapshot', 'ts quadrant data')

VERBOSE = False

@click.command()
@click.help_option('-h', '--help')
@click.option('-t', '--table', default='tiles', show_default=True,
              help='HDF5 table name for the tile placement table')
@click.option('-v', '--verbose', is_flag=True)
@click.argument('hdf5', type=click.Path(exists=True, dir_okay=False))
@click.argument('canvasfile', nargs=-1, type=click.Path(exists=True))
def main(hdf5, canvasfile, table, verbose):
    global VERBOSE
    VERBOSE = verbose
    csv.field_size_limit(2 * 1024 * 1024)
    canvasfile = [pathlib.Path(p) for p in canvasfile]
    tiles = load_tile_data(hdf5, table)
    snapshots = load_snapshot_data(canvasfile)
    new_placements = add_placements(tiles, snapshots)
    if VERBOSE:
        print('added placements:')
        print(new_placements)
    merged = merge_placements(tiles, new_placements)
    save_merged_placements(merged, hdf5, table)
    

def int_to_timestamp(ts):
    return datetime.datetime.utcfromtimestamp(ts / 1000)


def load_tile_data(hdf5, table):
    with TimedAction('loading HDF5'):
        tiles = pd.read_hdf(hdf5, table)
    if 'synthetic' in tiles.columns:
        with TimedAction('dropping old synthetic placements'):
            tiles = tiles[~tiles.synthetic]
            tiles = tiles.drop('synthetic', axis=1)
    return tiles


def load_snapshot_data(canvasfile):
    snapshots = []
    total_input_size = sum(d.stat().st_size for d in canvasfile)
    progress_bar = tqdm(total=total_input_size, desc='loading opl', position=0,
                        unit_scale=True, unit='B')
    for filename in canvasfile:
        read_file(filename, snapshots)
        progress_bar.update(filename.stat().st_size)
    progress_bar.close()
    return sorted(snapshots)

    
def read_file(filename, snapshots):
    if tarfile.is_tarfile(filename):
        read_file_tar(filename, snapshots)
    else:
        read_file_csv(filename, snapshots)


def read_file_tar(filename, snapshots):
    with tarfile.open(filename) as tf:
        for member in tqdm(tf, desc=filename.stem, position=1, leave=None,
                           unit='snap'):
            filename_parts = member.name.split('-')
            timestamp = int_to_timestamp(int(filename_parts[0]))
            quadrant = int(filename_parts[1])
            buf = tf.extractfile(member)
            snapshot = Snapshot(timestamp, quadrant, buf.read())
            snapshots.append(snapshot)

    
def read_file_csv(filename, snapshots):
    lines = linecount(filename, progress=1)
    with lzma.open(filename, 'rt') as cf:
        reader = csv.DictReader(cf, ['timestamp', 'label', 'author', 'data'])
        for row in tqdm(reader, total=lines, position=1, leave=None,
                        desc=filename.stem):
            add_row(row, snapshots)


def add_row(row, snapshots):
    timestamp = int_to_timestamp(int(row['timestamp']))
    canvas, mode = label_to_canvas(row['label'], timestamp)
    if canvas is None:
        return
    # Skip differential updates for now.
    if mode == 'diff':
        return

    data = base64.b64decode(row['data'])
    if data == b'{"message":"Rate exceeded"}':
        # Common message; ignore
        return
    if not data.startswith(PNG_SIGNATURE):
        tqdm.write('{} {} {}'.format(
            timestamp, row['label'], data.decode('utf8')))
        return

    snapshots.append(Snapshot(timestamp, canvas, data))


def label_to_canvas(label, timestamp):
    # Split into chunks
    label_parts = label.split('.')

    # Parse chunks literally
    assert label_parts[0] == 'canvasDownloader'
    if len(label_parts) == 1:
        canvas = 0
        mode = 'diff'
    elif len(label_parts) == 2:
        if label_parts[1] == 'full':
            canvas = 0
            mode = 'full'
        else:
            canvas = int(label_parts[1])
            mode = 'diff'
    else:
        assert len(label_parts) == 3
        assert label_parts[2] == 'full'
        canvas = int(label_parts[1])
        mode = 'full'

    # Account for data problems
    if int_to_timestamp(1648917727146) <= timestamp \
       and timestamp <= int_to_timestamp(1648917896249):
        # Canvases 0 and 1 were both counted as canvas 0.  Punt on the
        # problem, and let the data for this timespan ceom from the
        # redownloads.
        return None, None
    if int_to_timestamp(1648917896543) <= timestamp \
       and timestamp <= int_to_timestamp(1648918568944):
        # The downloader used different IDs for the different modes, and
        # the IDs did not align to the affected canvases.
        if canvas == 2:
            canvas = 0
            mode = 'diff'
        elif canvas == 3:
            canvas = 0
            mode = 'full'
        elif canvas == 4:
            canvas = 1
            mode = 'diff'
        elif canvas == 5:
            canvas = 1
            mode = 'full'
        else:
            tqdm.write('Unexpected canvas in first expansion error area: ' + label)
            return None, None

    # Return the values
    return canvas, mode


def next_or_none(iterator, values=1):
    try:
        return next(iterator)
    except StopIteration:
        if values == 1:
            return None
        else:
            return [None] * values


def add_placements(tiles, snapshots):
    new_placements = {
        'ts': [],
        'x': [],
        'y': [],
        'color': [],
    }
    year = tiles.ts[0].year
    init_palette(year)
    canvas = init_canvas(year)
    iter_p = group_placements_for_snapshots(tiles, snapshots)
    pb_p = tqdm(total=len(tiles), position=0, desc='placements', unit='p')
    iter_s = iterate_snapshots(snapshots)
    next_snapshot = next_or_none(iter_s)
    _, next_placements = next_or_none(iter_p, 2)
    while next_snapshot is not None:
        while next_placements is not None and len(next_placements) == 0:
            _, next_placements = next_or_none(iter_p, 2)
        if next_placements is not None and next_placements.index[0] <= next_snapshot.ts:
            relevant_placements = next_placements.groupby(['x', 'y'], as_index=False)['color'].last()
            canvas[relevant_placements.y, relevant_placements.x] = relevant_placements.color
            pb_p.update(len(next_placements))
            _, next_placements = next_or_none(iter_p, 2)
        elif next_snapshot is not None:
            apply_snapshot(canvas, next_snapshot, new_placements)
            next_snapshot = next_or_none(iter_s)
    pb_p.close()
    result = pd.DataFrame(new_placements)
    result['user_hash'] = ''
    result['mod_action'] = False
    result['synthetic'] = True
    return result


def group_placements_for_snapshots(tiles, snapshots):
    with TimedAction('grouping placements'):
        # All time intervals.  Subtract a bit from the smallest timestamp
        # so it'll be properly included in the half-open interval
        timestamps = pd.Series(sorted(
            [tiles.ts.iloc[0] - datetime.timedelta(seconds=1), tiles.ts.iloc[-1]] +
            [s.ts for s in snapshots])).unique()
        timestamp_intervals = pd.IntervalIndex.from_breaks(timestamps)
        tiles_indexed = tiles.set_index('ts')
        grouping = pd.cut(tiles_indexed.index, bins=timestamp_intervals)
        return tiles_indexed.groupby(grouping, observed=True).__iter__()

def init_canvas(year):
    return np.full(list(reversed(CANVAS_SIZE[year])), COLOR_WHITE_INDEX[year])


def iterate_snapshots(snapshots):
    for snapshot in tqdm(snapshots, position=1, desc='snapshots', unit='snap'):
        yield snapshot


def apply_snapshot(canvas, snapshot, new_placements):
    reader = png.Reader(bytes=snapshot.data)
    width, height, rows, info = reader.asRGBA8()
    snapshot_rgb = np.array(list(rows)).reshape(width, height, -1)
    snapshot_pal = apply_palette(snapshot_rgb)
    x0 = 1000 * (snapshot.quadrant % 2)
    y0 = 1000 * (snapshot.quadrant // 2)
    mismatched_indices = np.argwhere(
        np.logical_and(
            snapshot_pal != COLOR_MISSING_INDEX[snapshot.ts.year],
            snapshot_pal != canvas[y0:y0+1000, x0:x0+1000]))
    for dy, dx in mismatched_indices:
        x = x0+dx
        y = y0+dy
        if VERBOSE:
            tqdm.write('{} ({},{}) {} -> {}'.format(
                snapshot.ts, x, y, canvas[y, x], snapshot_pal[dy, dx]))
        canvas[y, x] = snapshot_pal[dy, dx]
        new_placements['ts'].append(snapshot.ts)
        new_placements['x'].append(x)
        new_placements['y'].append(y)
        new_placements['color'].append(canvas[y, x])


def merge_placements(old, new):
    with TimedAction('merging placements'):
        old.user_hash.cat.add_categories('')
        old['synthetic'] = False
        merged = pd.concat([old, new], ignore_index=True)
        sort_columns = ['ts', 'y', 'x']
        order = np.lexsort(
            [merged[col].values for col in reversed(sort_columns)])
        for col in list(merged.columns):
            merged[col] = merged[col].values[order]
    with TimedAction('fixing dtypes'):
        dtypes = {
            'x': np.uint16,
            'y': np.uint16,
            'color': np.uint8,
            'user_hash': 'category',
        }
        for col, dtype in dtypes.items():
            merged[col] = merged[col].astype(dtype)
    return merged


def save_merged_placements(merged, hdf5, table):
    with TimedAction('saving placements'):
        merged.to_hdf(hdf5, table, format='table', data_columns=True)


### Functions for applying the palette to an image

COLOR_TO_INT_MATRIX = None
INVERSE_PALETTE = None
INVERSE_PALETTE_INDEX = None
COLLAPSED_CANVAS = None  # Used and reused to avoid consing

def init_palette(year):
    global COLOR_TO_INT_MATRIX, INVERSE_PALETTE, INVERSE_PALETTE_INDEX, \
        COLLAPSED_CANVAS
    COLOR_TO_INT_MATRIX = \
        (1 + COLORS[year].max())**np.arange(COLORS[year].shape[-1])
    INVERSE_PALETTE, INVERSE_PALETTE_INDEX = \
        np.unique(COLORS[year] @ COLOR_TO_INT_MATRIX, return_index=True)
    COLLAPSED_CANVAS = np.empty((1000, 1000), np.int64)

def apply_palette(canvas):
    np.matmul(canvas, COLOR_TO_INT_MATRIX, out=COLLAPSED_CANVAS)
    return INVERSE_PALETTE_INDEX[
        np.searchsorted(INVERSE_PALETTE, COLLAPSED_CANVAS)]


if __name__ == '__main__':
    main()
